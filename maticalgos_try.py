# maticalgos==0.70
from maticalgos.historical import historical
from date_util import parse_date_to_date_time, today, one_day_delta

ma = historical('workwithtradenvest@gmail.com')
ma.login('889884')

dateformat = '%Y%m%d'

def fetch(symbol, start_date, end_date=None):
    start_date_time = parse_date_to_date_time(start_date)
    end_date_time = parse_date_to_date_time(end_date, default=today())
    ith_date_time = start_date_time
    while ith_date_time <= end_date_time:
        data = ma.get_data(symbol, start_date_time)
        print(data.head(3))
        ith_date_time += one_day_delta

fetch("ICICIBANK", "2023-07-04")
