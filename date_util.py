import datetime
from typing import Optional

today = datetime.date.today
one_day_delta = datetime.timedelta(days=1)

def parse_date_to_date_time(
        date_str: Optional[str],
        date_format: str='%Y-%m-%d',
        default: Optional[datetime.datetime]=None,
        eod: bool= False) -> Optional[datetime.datetime]:
    # need to work on this logic
    if not date_str:
        return default
    dt = datetime.datetime.strptime(date_str, date_format)
    if eod:
        dt = dt.replace(hour=23, minute=59, second=59, microsecond=999999)
    return dt


def parse_date_time_to_date(
        dt: datetime.datetime,
        date_format: str='%Y-%m-%d',
) -> str:
    return dt.strftime(date_format)