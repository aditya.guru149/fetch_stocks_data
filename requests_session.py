from pyrate_limiter import Duration, Limiter, MemoryQueueBucket, RequestRate
from requests import Session
from requests_cache import CacheMixin, SQLiteCache
from requests_ratelimiter import LimiterMixin



class CachedLimiterSession(CacheMixin, LimiterMixin, Session):
    pass
    # headers['User-Agent'] = 'Firefox'


limiter_session = CachedLimiterSession(
    limiter=Limiter(RequestRate(2, Duration.SECOND*10)),  # max 2 requests per 10 seconds
    bucket_class=MemoryQueueBucket,
    backend=SQLiteCache("yfinance.cache"),
)
