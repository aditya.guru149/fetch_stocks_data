# https://analyzingalpha.com/yfinance-python
# https://github.com/ranaroussi/yfinance#smarter-scraping

import yfinance as yf

from date_util import parse_date_to_date_time, today
from requests_session import limiter_session

def fetch(symbol, start_date, end_date=None, interval='1d', save_as='csv'):
    start_date_time = parse_date_to_date_time(start_date)
    end_date_time = parse_date_to_date_time(end_date, default=today(), eod=True)
    data = yf.download(
        symbol, start=start_date_time, end=end_date_time, interval=interval, session=limiter_session)
    print(data.shape)
    null_rows = data[data.isna().any(axis=1)]
    if not null_rows.empty:
        print(null_rows)
    print(symbol)
    print(data.tail(3))
    if not data.empty:
        if save_as == 'csv':
            file_name = f"out/{interval}/{symbol}-{start_date}-{end_date}.csv"
            data.to_csv(file_name)

symbols_list = ['^NSEI', '^NSEBANK', '^CNXIT', '^CNXAUTO'] + ['RELIANCE.NS', 'HDFCBANK.NS', 'ICICIBANK.NS', 'INFY.NS', 'TCS.NS']
# symbols_list = ['RELIANCE.NS']
for symbol in symbols_list:
    fetch(symbol, '2024-11-03', '2024-12-14', interval='5m')
