import pandas as pd
import yfinance as yf

from dataclasses import dataclass
from date_util import parse_date_time_to_date, today
from requests_session import limiter_session

@dataclass
class EarningsDFWrapper:
    all_dfs = []

    def append(self, symbol: str, earnings_df):
        symbol_earnings_df = earnings_df.copy(deep=True)
        if symbol_earnings_df is None:
            return
        symbol_earnings_df.dropna(inplace=True, how="all")         
        if symbol_earnings_df.empty:
            return
        symbol_earnings_df['Company'] = symbol
        symbol_earnings_df.set_index('Company', inplace=True, append=True)
        symbol_earnings_df = symbol_earnings_df.swaplevel(0, 1)
        return self.all_dfs.append(symbol_earnings_df)
    
    def save_to_csv(self):
        earnings_df = pd.concat(self.all_dfs)
        file_name = f"out/result_dates_{parse_date_time_to_date(today())}.csv"
        if not earnings_df.empty:
            earnings_df.to_csv(file_name)
            pass


def fetch(symbols):
    edfw = EarningsDFWrapper()
    for sym in symbols:
        df = yf.Ticker(sym, session=limiter_session).get_earnings_dates()
        edfw.append(sym, df)
    edfw.save_to_csv()
        

symbols_list = ['RELIANCE.NS', 'HDFCBANK.NS', 'ICICIBANK.NS', 'INFY.NS', 'TCS.NS']
# symbols_list = ['AAPL', 'MSFT']
fetch(symbols_list)
